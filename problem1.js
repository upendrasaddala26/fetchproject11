

let fetch = require("node-fetch");


let url =" https://jsonplaceholder.typicode.com/users";


function first(){
    let value=fetch(url);
    return new Promise((res,rej)=>{
        res(value)
        if(value===null || value===undefined || value===""){
            rej("Data not found")
        }
        
    })
}
first().then((data)=>{
    return data.json()
}).then((data)=>console.log(data)).catch((err)=>{
    console.log(err);
})

